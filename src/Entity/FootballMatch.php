<?php

namespace App\Entity;

use App\Repository\FootballMatchRepository;
use App\Traits\Timestamps;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FootballMatchRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class FootballMatch
{
    use Timestamps;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $matchDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stadium;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $localScoreboard;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $awayScoreboard;

    /**
     * @ORM\ManyToOne(targetEntity=Season::class, inversedBy="footballMatches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $season;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isLosGarresLocal;

    /**
     * @ORM\ManyToOne(targetEntity=Team::class, inversedBy="matches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $losGarresTeam;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $visitingTeamName;

    /**
     * @ORM\ManyToMany(targetEntity=Player::class, mappedBy="matches")
     */
    private $losGarresPlayers;

    /**
     * @ORM\OneToMany(targetEntity=VisitingPlayer::class, mappedBy="footballMatch", orphanRemoval=true, cascade={"persist"})
     */
    private $visitingPlayers;

    public function __construct()
    {
        $this->losGarresPlayers = new ArrayCollection();
        $this->visitingPlayers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchDate(): ?\DateTimeInterface
    {
        return $this->matchDate;
    }

    public function setMatchDate(?\DateTimeInterface $matchDate): self
    {
        $this->matchDate = $matchDate;

        return $this;
    }

    public function getStadium(): ?string
    {
        return $this->stadium;
    }

    public function setStadium(string $stadium): self
    {
        $this->stadium = $stadium;

        return $this;
    }

    public function getLocalScoreboard(): ?int
    {
        return $this->localScoreboard;
    }

    public function setLocalScoreboard(?int $localScoreboard): self
    {
        $this->localScoreboard = $localScoreboard;

        return $this;
    }

    public function getAwayScoreboard(): ?int
    {
        return $this->awayScoreboard;
    }

    public function setAwayScoreboard(?int $awayScoreboard): self
    {
        $this->awayScoreboard = $awayScoreboard;

        return $this;
    }

    public function getSeason(): ?Season
    {
        return $this->season;
    }

    public function setSeason(?Season $season): self
    {
        $this->season = $season;

        return $this;
    }

    public function getIsLosGarresLocal(): ?bool
    {
        return $this->isLosGarresLocal;
    }

    public function setIsLosGarresLocal(bool $isLosGarresLocal): self
    {
        $this->isLosGarresLocal = $isLosGarresLocal;

        return $this;
    }

    public function getLosGarresTeam(): ?Team
    {
        return $this->losGarresTeam;
    }

    public function setLosGarresTeam(?Team $losGarresTeam): self
    {
        $this->losGarresTeam = $losGarresTeam;

        return $this;
    }

    public function getVisitingTeamName(): ?string
    {
        return $this->visitingTeamName;
    }

    public function setVisitingTeamName(string $visitingTeamName): self
    {
        $this->visitingTeamName = $visitingTeamName;

        return $this;
    }

    /**
     * @return Collection|Player[]
     */
    public function getLosGarresPlayers(): Collection
    {
        return $this->losGarresPlayers;
    }

    public function addLosGarresPlayer(Player $losGarresPlayer): self
    {
        if (!$this->losGarresPlayers->contains($losGarresPlayer)) {
            $this->losGarresPlayers[] = $losGarresPlayer;
            $losGarresPlayer->addMatch($this);
        }

        return $this;
    }

    public function removeLosGarresPlayer(Player $losGarresPlayer): self
    {
        if ($this->losGarresPlayers->removeElement($losGarresPlayer)) {
            $losGarresPlayer->removeMatch($this);
        }

        return $this;
    }

    /**
     * @return Collection|VisitingPlayer[]
     */
    public function getVisitingPlayers(): Collection
    {
        return $this->visitingPlayers;
    }

    public function addVisitingPlayer(VisitingPlayer $visitingPlayer): self
    {
        if (!$this->visitingPlayers->contains($visitingPlayer)) {
            $this->visitingPlayers[] = $visitingPlayer;
            $visitingPlayer->setFootballMatch($this);
        }

        return $this;
    }

    public function removeVisitingPlayer(VisitingPlayer $visitingPlayer): self
    {
        if ($this->visitingPlayers->removeElement($visitingPlayer)) {
            // set the owning side to null (unless already changed)
            if ($visitingPlayer->getFootballMatch() === $this) {
                $visitingPlayer->setFootballMatch(null);
            }
        }

        return $this;
    }
}
