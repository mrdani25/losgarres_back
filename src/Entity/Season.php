<?php

namespace App\Entity;

use App\Repository\SeasonRepository;
use App\Traits\Timestamps;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SeasonRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Season
{
    use Timestamps;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\OneToMany(targetEntity=FootballMatch::class, mappedBy="season")
     */
    private $footballMatches;

    public function __construct()
    {
        $this->footballMatches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Collection|FootballMatch[]
     */
    public function getFootballMatches(): Collection
    {
        return $this->footballMatches;
    }

    public function addFootballMatch(FootballMatch $footballMatch): self
    {
        if (!$this->footballMatches->contains($footballMatch)) {
            $this->footballMatches[] = $footballMatch;
            $footballMatch->setSeason($this);
        }

        return $this;
    }

    public function removeFootballMatch(FootballMatch $footballMatch): self
    {
        if ($this->footballMatches->removeElement($footballMatch)) {
            // set the owning side to null (unless already changed)
            if ($footballMatch->getSeason() === $this) {
                $footballMatch->setSeason(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
