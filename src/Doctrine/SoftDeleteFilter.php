<?php

namespace App\Doctrine;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class SoftDeleteFilter extends SQLFilter
{

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (!array_key_exists('soft_delete', $targetEntity->fieldNames)) {
            return '';
        }
        return sprintf('%s.soft_delete = false', $targetTableAlias);
    }
}