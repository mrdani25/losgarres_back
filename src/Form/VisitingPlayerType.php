<?php

namespace App\Form;

use App\Entity\VisitingPlayer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VisitingPlayerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('position', ChoiceType::class, [
                'choices' => [
                    'Portero' => 'Portero',
                    'Defensa' => 'Defensa',
                    'Medio' => 'Medio',
                    'Delantero' => 'Delantero'
                ],
                'multiple' => false,
                'expanded' => true,
                'required' => true
            ])
            ->add('dorsal', IntegerType::class, [
                'attr' => [
                    'min' => 0
                ],
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VisitingPlayer::class,
        ]);
    }
}
