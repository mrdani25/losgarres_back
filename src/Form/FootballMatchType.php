<?php

namespace App\Form;

use App\Entity\FootballMatch;
use App\Entity\Player;
use App\Entity\Season;
use App\Entity\Team;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class FootballMatchType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('matchDate', DateTimeType::class, [
                'label' => $this->translator->trans('Match date'),
                'time_widget' => 'single_text',
                'date_widget' => 'single_text',
                'required' => false,
            ])
            ->add('stadium', TextType::class, [
                'required' => true
            ])
            ->add('localScoreboard', IntegerType::class, [
                'required' => false,
                'attr' => [
                    'min' => 0
                ],
                'label' => $this->translator->trans('Local scoreboard')
            ])
            ->add('awayScoreboard', IntegerType::class, [
                'required' => false,
                'attr' => [
                    'min' => 0
                ],
                'label' => $this->translator->trans('Away scoreboard')
            ])
            ->add('season', EntityType::class, [
                'class' => Season::class,
                'choice_label' => 'name',
                'multiple' => false,
                'required' => true
            ])
            ->add('isLosGarresLocal', ChoiceType::class, [
                'label' => $this->translator->trans('Local or visitor?'),
                'choices' => [
                    'Local' => '1',
                    'Visitante' => '0'
                ],
                'multiple' => false,
                'expanded' => false,
                'required' => true
            ])
            ->add('losGarresTeam', EntityType::class, [
                'label' => $this->translator->trans('Los Garres team'),
                'class' => Team::class,
                'choice_label' => 'name',
                'multiple' => false,
                'required' => true
            ])
            ->add('visitingTeamName', TextType::class, [
                'required' => true,
                'label' => $this->translator->trans('Visiting team name')
            ])
            ->add('losGarresPlayers', EntityType::class, [
                'class' => Player::class,
                'expanded' =>  true,
                'multiple' => true,
                'label' => $this->translator->trans('Los garres players')
            ])
            ->add('visitingPlayers', CollectionType::class, [
                'entry_type' => VisitingPlayerType::class,
                'by_reference' => false,
                'allow_delete' => true,
                'allow_add' => true,
                'label' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FootballMatch::class,
        ]);
    }
}
