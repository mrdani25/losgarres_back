<?php

namespace App\Form;

use App\Entity\Staff;
use App\Entity\Team;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class TeamType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('category', TextType::class, [
                'required' => true
            ])
            ->add('visibleName', TextType::class, [
                'required' => true
            ])
            ->add('shortName', TextType::class, [
                'required' => true,
                'label' => $this->translator->trans('Short name')
            ])
            ->add('completeName', TextType::class, [
                'required' => true,
                'label' => $this->translator->trans('Complete name')
            ])
            ->add('staffs', EntityType::class, [
                'class' => Staff::class,
                'label' => $this->translator->trans('Staffs'),
                'multiple' => true,
                'expanded' => true,
                'required' => false,
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => false,
                'required' => false,
                'allow_delete' => true,
                'delete_label' => 'delete',
                'download_label' => 'Download',
                'download_uri' => true,
                'image_uri' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Team::class,
        ]);
    }
}
