<?php

namespace App\Form;

use App\Entity\Staff;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StaffType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('surname1', TextType::class, [
                'required' => true
            ])
            ->add('surname2', TextType::class, [
                'required' => false
            ])
            ->add('visibleName', TextType::class, [
                'required' => true
            ])
            ->add('job', ChoiceType::class, [
                'choices' => [
                    'Primer entrenador' => 'Primer entrenador',
                    'Segundo entrenador' => 'Segundo entrenador',
                    'Entrenador de porteros' => 'Entrenador de porteros',
                    'Preparador físico' => 'Preparador físico',
                    'Delegado' => 'Delegado',
                    'Nutricionista' => 'Nutricionista',
                    'Analista' => 'Analista',
                    'Utilero' => 'Utilero',
                    'Fisioterapeuta' => 'Fisioterapeuta',
                    'Psicólogo' => 'Psicólogo'
                ],
                'multiple' => false,
                'expanded' => true,
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Staff::class,
        ]);
    }
}
