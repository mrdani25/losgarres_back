<?php

namespace App\Form;

use App\Entity\Player;
use App\Entity\Team;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class PlayerType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('surname1', TextType::class, [
                'required' => true
            ])
            ->add('surname2', TextType::class, [
                'required' => false
            ])
            ->add('visibleName', TextType::class, [
                'required' => true
            ])
            ->add('nickname', TextType::class, [
                'required' => true
            ])
            ->add('position', ChoiceType::class, [
                'choices' => [
                    'Portero' => 'Portero',
                    'Defensa' => 'Defensa',
                    'Medio' => 'Medio',
                    'Delantero' => 'Delantero'
                ],
                'multiple' => false,
                'expanded' => true,
                'required' => true
            ])
            ->add('birthDate', DateTimeType::class, [
                'label' => $this->translator->trans('Birth date'),
                'time_widget' => 'single_text',
                'date_widget' => 'single_text',
                'required' => false,
            ])
            ->add('playerInfo', TextareaType::class, [
                'required' => false
            ])
            ->add('dorsal', IntegerType::class, [
                'attr' => [
                    'min' => 0
                ],
                'required' => true
            ])
            ->add('team', EntityType::class, [
                'class' => Team::class,
                'multiple' => false,
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Player::class,
        ]);
    }
}
