<?php


namespace App\Vich;

use App\Entity\Team;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class DirectoryNamer implements DirectoryNamerInterface
{

    public function directoryName($object, PropertyMapping $mapping): string
    {
        if($object instanceof Team) {
            $data = str_split($object->getId(),1);
            return implode('/', $data);
        }

        return '';
    }
}