<?php

namespace App\Controller;

use App\Entity\VisitingPlayer;
use App\Form\VisitingPlayerType;
use App\Repository\VisitingPlayerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/visiting/player")
 */
class VisitingPlayerController extends AbstractController
{
    /**
     * @Route("/", name="visiting_player_index", methods={"GET"})
     */
    public function index(VisitingPlayerRepository $visitingPlayerRepository): Response
    {
        return $this->render('visiting_player/index.html.twig', [
            'visiting_players' => $visitingPlayerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="visiting_player_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $visitingPlayer = new VisitingPlayer();
        $form = $this->createForm(VisitingPlayerType::class, $visitingPlayer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($visitingPlayer);
            $entityManager->flush();

            return $this->redirectToRoute('visiting_player_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('visiting_player/new.html.twig', [
            'visiting_player' => $visitingPlayer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="visiting_player_show", methods={"GET"})
     */
    public function show(VisitingPlayer $visitingPlayer): Response
    {
        return $this->render('visiting_player/show.html.twig', [
            'visiting_player' => $visitingPlayer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="visiting_player_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, VisitingPlayer $visitingPlayer): Response
    {
        $form = $this->createForm(VisitingPlayerType::class, $visitingPlayer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('visiting_player_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('visiting_player/edit.html.twig', [
            'visiting_player' => $visitingPlayer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="visiting_player_delete", methods={"POST"})
     */
    public function delete(Request $request, VisitingPlayer $visitingPlayer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$visitingPlayer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $visitingPlayer->setSoftDelete(true);
            $entityManager->persist($visitingPlayer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('visiting_player_index', [], Response::HTTP_SEE_OTHER);
    }
}
