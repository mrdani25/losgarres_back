<?php

namespace App\Controller;

use App\Entity\FootballMatch;
use App\Form\FootballMatchType;
use App\Repository\FootballMatchRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/match")
 */
class FootballMatchController extends AbstractController
{
    /**
     * @Route("/", name="football_match_index", methods={"GET"})
     */
    public function index(FootballMatchRepository $footballMatchRepository): Response
    {
        return $this->render('football_match/index.html.twig', [
            'football_matches' => $footballMatchRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="football_match_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $footballMatch = new FootballMatch();
        $form = $this->createForm(FootballMatchType::class, $footballMatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($footballMatch);
            $entityManager->flush();

            return $this->redirectToRoute('football_match_index');
        }

        return $this->render('football_match/new.html.twig', [
            'football_match' => $footballMatch,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="football_match_show", methods={"GET"})
     */
    public function show(FootballMatch $footballMatch): Response
    {
        return $this->render('football_match/show.html.twig', [
            'football_match' => $footballMatch,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="football_match_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FootballMatch $footballMatch): Response
    {
        $form = $this->createForm(FootballMatchType::class, $footballMatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('football_match_index');
        }

        return $this->render('football_match/edit.html.twig', [
            'football_match' => $footballMatch,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="football_match_delete", methods={"POST"})
     */
    public function delete(Request $request, FootballMatch $footballMatch): Response
    {
        if ($this->isCsrfTokenValid('delete'.$footballMatch->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $footballMatch->setSoftDelete(true);
            $entityManager->persist($footballMatch);
            $entityManager->flush();
        }

        return $this->redirectToRoute('football_match_index');
    }
}
