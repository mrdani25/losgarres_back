<?php

namespace App\Repository;

use App\Entity\VisitingPlayer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VisitingPlayerRepository|null find($id, $lockMode = null, $lockVersion = null)
 * @method VisitingPlayerRepository|null findOneBy(array $criteria, array $orderBy = null)
 * @method VisitingPlayerRepository[]    findAll()
 * @method VisitingPlayerRepository[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitingPlayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VisitingPlayerRepository::class);
    }

    // /**
    //  * @return VisitingPlayerRepository[] Returns an array of VisitingPlayerRepository objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VisitingPlayerRepository
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
