<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210619190136 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE team_match (id INT AUTO_INCREMENT NOT NULL, team_id INT DEFAULT NULL, team_name VARCHAR(255) DEFAULT NULL, INDEX IDX_BD5D8C45296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE team_match ADD CONSTRAINT FK_BD5D8C45296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE football_match ADD local_team_id INT NOT NULL, ADD visiting_team_id INT NOT NULL');
        $this->addSql('ALTER TABLE football_match ADD CONSTRAINT FK_8CE33ACEB4B9DD23 FOREIGN KEY (local_team_id) REFERENCES team_match (id)');
        $this->addSql('ALTER TABLE football_match ADD CONSTRAINT FK_8CE33ACE8897A92E FOREIGN KEY (visiting_team_id) REFERENCES team_match (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CE33ACEB4B9DD23 ON football_match (local_team_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CE33ACE8897A92E ON football_match (visiting_team_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE football_match DROP FOREIGN KEY FK_8CE33ACEB4B9DD23');
        $this->addSql('ALTER TABLE football_match DROP FOREIGN KEY FK_8CE33ACE8897A92E');
        $this->addSql('DROP TABLE team_match');
        $this->addSql('DROP INDEX UNIQ_8CE33ACEB4B9DD23 ON football_match');
        $this->addSql('DROP INDEX UNIQ_8CE33ACE8897A92E ON football_match');
        $this->addSql('ALTER TABLE football_match DROP local_team_id, DROP visiting_team_id');
    }
}
