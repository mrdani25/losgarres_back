<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211010182823 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE player_football_match (player_id INT NOT NULL, football_match_id INT NOT NULL, INDEX IDX_403AAD2799E6F5DF (player_id), INDEX IDX_403AAD27E1DA134D (football_match_id), PRIMARY KEY(player_id, football_match_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visiting_player (id INT AUTO_INCREMENT NOT NULL, football_match_id INT NOT NULL, name VARCHAR(255) NOT NULL, position VARCHAR(255) NOT NULL, dorsal INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, soft_delete TINYINT(1) NOT NULL, INDEX IDX_B2FB84C4E1DA134D (football_match_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE player_football_match ADD CONSTRAINT FK_403AAD2799E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE player_football_match ADD CONSTRAINT FK_403AAD27E1DA134D FOREIGN KEY (football_match_id) REFERENCES football_match (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE visiting_player ADD CONSTRAINT FK_B2FB84C4E1DA134D FOREIGN KEY (football_match_id) REFERENCES football_match (id)');
        $this->addSql('DROP TABLE football_match_player');
        $this->addSql('DROP TABLE player_match');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE football_match_player (football_match_id INT NOT NULL, player_id INT NOT NULL, INDEX IDX_9012E977E1DA134D (football_match_id), INDEX IDX_9012E97799E6F5DF (player_id), PRIMARY KEY(football_match_id, player_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE player_match (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, football_match_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, position VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, dorsal INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, soft_delete TINYINT(1) NOT NULL, INDEX IDX_C529BE43E1DA134D (football_match_id), INDEX IDX_C529BE4399E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE football_match_player ADD CONSTRAINT FK_9012E97799E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE football_match_player ADD CONSTRAINT FK_9012E977E1DA134D FOREIGN KEY (football_match_id) REFERENCES football_match (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE player_match ADD CONSTRAINT FK_C529BE4399E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player_match ADD CONSTRAINT FK_C529BE43E1DA134D FOREIGN KEY (football_match_id) REFERENCES football_match (id)');
        $this->addSql('DROP TABLE player_football_match');
        $this->addSql('DROP TABLE visiting_player');
    }
}
