<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210725114144 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE football_match DROP FOREIGN KEY FK_8CE33ACE8897A92E');
        $this->addSql('ALTER TABLE football_match DROP FOREIGN KEY FK_8CE33ACEB4B9DD23');
        $this->addSql('DROP INDEX UNIQ_8CE33ACEB4B9DD23 ON football_match');
        $this->addSql('DROP INDEX UNIQ_8CE33ACE8897A92E ON football_match');
        $this->addSql('ALTER TABLE football_match ADD los_garres_team_id INT NOT NULL, ADD opposing_team_id INT NOT NULL, DROP local_team_id, DROP visiting_team_id');
        $this->addSql('ALTER TABLE football_match ADD CONSTRAINT FK_8CE33ACE975D3E7D FOREIGN KEY (los_garres_team_id) REFERENCES team_match (id)');
        $this->addSql('ALTER TABLE football_match ADD CONSTRAINT FK_8CE33ACE29C702C7 FOREIGN KEY (opposing_team_id) REFERENCES team_match (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CE33ACE975D3E7D ON football_match (los_garres_team_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CE33ACE29C702C7 ON football_match (opposing_team_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE football_match DROP FOREIGN KEY FK_8CE33ACE975D3E7D');
        $this->addSql('ALTER TABLE football_match DROP FOREIGN KEY FK_8CE33ACE29C702C7');
        $this->addSql('DROP INDEX UNIQ_8CE33ACE975D3E7D ON football_match');
        $this->addSql('DROP INDEX UNIQ_8CE33ACE29C702C7 ON football_match');
        $this->addSql('ALTER TABLE football_match ADD local_team_id INT NOT NULL, ADD visiting_team_id INT NOT NULL, DROP los_garres_team_id, DROP opposing_team_id');
        $this->addSql('ALTER TABLE football_match ADD CONSTRAINT FK_8CE33ACE8897A92E FOREIGN KEY (visiting_team_id) REFERENCES team_match (id)');
        $this->addSql('ALTER TABLE football_match ADD CONSTRAINT FK_8CE33ACEB4B9DD23 FOREIGN KEY (local_team_id) REFERENCES team_match (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CE33ACEB4B9DD23 ON football_match (local_team_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CE33ACE8897A92E ON football_match (visiting_team_id)');
    }
}
