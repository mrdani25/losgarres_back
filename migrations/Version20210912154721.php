<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210912154721 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE football_match_player (football_match_id INT NOT NULL, player_id INT NOT NULL, INDEX IDX_9012E977E1DA134D (football_match_id), INDEX IDX_9012E97799E6F5DF (player_id), PRIMARY KEY(football_match_id, player_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player_match (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, football_match_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, position VARCHAR(255) NOT NULL, dorsal INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, soft_delete TINYINT(1) NOT NULL, INDEX IDX_C529BE4399E6F5DF (player_id), INDEX IDX_C529BE43E1DA134D (football_match_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE football_match_player ADD CONSTRAINT FK_9012E977E1DA134D FOREIGN KEY (football_match_id) REFERENCES football_match (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE football_match_player ADD CONSTRAINT FK_9012E97799E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE player_match ADD CONSTRAINT FK_C529BE4399E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player_match ADD CONSTRAINT FK_C529BE43E1DA134D FOREIGN KEY (football_match_id) REFERENCES football_match (id)');
        $this->addSql('DROP TABLE player_match');
        $this->addSql('ALTER TABLE football_match ADD is_los_garres_local TINYINT(1) NOT NULL, ADD visiting_team_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE team ADD matches_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F4B30DD19 FOREIGN KEY (matches_id) REFERENCES football_match (id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F4B30DD19 ON team (matches_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE player_match (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, position VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, dorsal INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, soft_delete TINYINT(1) NOT NULL, INDEX IDX_E2F8087899E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE player_match ADD CONSTRAINT FK_E2F8087899E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('DROP TABLE football_match_player');
        $this->addSql('DROP TABLE player_match');
        $this->addSql('ALTER TABLE football_match DROP is_los_garres_local, DROP visiting_team_name');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F4B30DD19');
        $this->addSql('DROP INDEX IDX_C4E0A61F4B30DD19 ON team');
        $this->addSql('ALTER TABLE team DROP matches_id');
    }
}
