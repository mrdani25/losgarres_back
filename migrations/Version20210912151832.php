<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210912151832 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE football_match DROP FOREIGN KEY FK_8CE33ACE29C702C7');
        $this->addSql('ALTER TABLE football_match DROP FOREIGN KEY FK_8CE33ACE975D3E7D');
        $this->addSql('ALTER TABLE player_match DROP FOREIGN KEY FK_E2F8087892FC70A8');
        $this->addSql('DROP TABLE team_match');
        $this->addSql('DROP INDEX UNIQ_8CE33ACE975D3E7D ON football_match');
        $this->addSql('DROP INDEX UNIQ_8CE33ACE29C702C7 ON football_match');
        $this->addSql('ALTER TABLE football_match DROP los_garres_team_id, DROP opposing_team_id');
        $this->addSql('DROP INDEX IDX_E2F8087892FC70A8 ON player_match');
        $this->addSql('ALTER TABLE player_match DROP team_match_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE team_match (id INT AUTO_INCREMENT NOT NULL, team_id INT DEFAULT NULL, team_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, soft_delete TINYINT(1) NOT NULL, INDEX IDX_BD5D8C45296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE team_match ADD CONSTRAINT FK_BD5D8C45296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE football_match ADD los_garres_team_id INT NOT NULL, ADD opposing_team_id INT NOT NULL');
        $this->addSql('ALTER TABLE football_match ADD CONSTRAINT FK_8CE33ACE29C702C7 FOREIGN KEY (opposing_team_id) REFERENCES team_match (id)');
        $this->addSql('ALTER TABLE football_match ADD CONSTRAINT FK_8CE33ACE975D3E7D FOREIGN KEY (los_garres_team_id) REFERENCES team_match (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CE33ACE975D3E7D ON football_match (los_garres_team_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CE33ACE29C702C7 ON football_match (opposing_team_id)');
        $this->addSql('ALTER TABLE player_match ADD team_match_id INT NOT NULL');
        $this->addSql('ALTER TABLE player_match ADD CONSTRAINT FK_E2F8087892FC70A8 FOREIGN KEY (team_match_id) REFERENCES team_match (id)');
        $this->addSql('CREATE INDEX IDX_E2F8087892FC70A8 ON player_match (team_match_id)');
    }
}
