<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210717130136 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE football_match ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD soft_delete TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE player ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD soft_delete TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE player_match ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD soft_delete TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE season ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD soft_delete TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE staff ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD soft_delete TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE team ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD soft_delete TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE team_match ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD soft_delete TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE football_match DROP created_at, DROP updated_at, DROP soft_delete');
        $this->addSql('ALTER TABLE player DROP created_at, DROP updated_at, DROP soft_delete');
        $this->addSql('ALTER TABLE player_match DROP created_at, DROP updated_at, DROP soft_delete');
        $this->addSql('ALTER TABLE season DROP created_at, DROP updated_at, DROP soft_delete');
        $this->addSql('ALTER TABLE staff DROP created_at, DROP updated_at, DROP soft_delete');
        $this->addSql('ALTER TABLE team DROP created_at, DROP updated_at, DROP soft_delete');
        $this->addSql('ALTER TABLE team_match DROP created_at, DROP updated_at, DROP soft_delete');
    }
}
