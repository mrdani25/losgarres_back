<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210619193612 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE player_match (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, team_match_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, position VARCHAR(255) NOT NULL, dorsal INT NOT NULL, INDEX IDX_E2F8087899E6F5DF (player_id), INDEX IDX_E2F8087892FC70A8 (team_match_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE player_match ADD CONSTRAINT FK_E2F8087899E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player_match ADD CONSTRAINT FK_E2F8087892FC70A8 FOREIGN KEY (team_match_id) REFERENCES team_match (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE player_match');
    }
}
