<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210912190234 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE football_match ADD los_garres_team_id INT NOT NULL');
        $this->addSql('ALTER TABLE football_match ADD CONSTRAINT FK_8CE33ACE975D3E7D FOREIGN KEY (los_garres_team_id) REFERENCES team (id)');
        $this->addSql('CREATE INDEX IDX_8CE33ACE975D3E7D ON football_match (los_garres_team_id)');
        $this->addSql('ALTER TABLE team DROP FOREIGN KEY FK_C4E0A61F4B30DD19');
        $this->addSql('DROP INDEX IDX_C4E0A61F4B30DD19 ON team');
        $this->addSql('ALTER TABLE team DROP matches_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE football_match DROP FOREIGN KEY FK_8CE33ACE975D3E7D');
        $this->addSql('DROP INDEX IDX_8CE33ACE975D3E7D ON football_match');
        $this->addSql('ALTER TABLE football_match DROP los_garres_team_id');
        $this->addSql('ALTER TABLE team ADD matches_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE team ADD CONSTRAINT FK_C4E0A61F4B30DD19 FOREIGN KEY (matches_id) REFERENCES football_match (id)');
        $this->addSql('CREATE INDEX IDX_C4E0A61F4B30DD19 ON team (matches_id)');
    }
}
