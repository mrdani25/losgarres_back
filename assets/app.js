/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
import './styles/app.scss';
import 'bootstrap';
import './scripts/main';

const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');


// ADD/REMOVE COLLECTIONS TO FORMS
var $tagsCollectionHolder = $('ul.tags');
// count the current form inputs we have (e.g. 2), use that as the new
// index when inserting a new item (e.g. 2)
$tagsCollectionHolder.data('index', $tagsCollectionHolder.find('input').length);

$('body').on('click', '.add_item_link', function (e) {
    var $collectionHolderClass = $(e.currentTarget).data('collectionHolderClass');
    // add a new tag form (see next code block)
    addFormToCollection($collectionHolderClass);
})

$("td").filter(function () {
    return $(this).text() === "No";
}).html('<span class="badge badge-danger">No</span>');

$("td").filter(function () {
    return $(this).text() === "Sí";
}).html('<span class="badge badge-success">Sí</span>');

$("#close-sidebar").click(function () {
    $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function () {
    $(".page-wrapper").addClass("toggled");
});
// Get the ul that holds the collection of tags
var $collectionHolder = $('ul.collection_form');

// add a delete link to all of the existing tag form li elements
$collectionHolder.find('li').each(function () {
    addFormDeleteLink($(this));
});

function addFormDeleteLink($tagFormLi) {
    var $removeFormButton = $('<button type="button" class="mt-3 mb-4 mr-2 btn-icon btn-shadow btn-outline-2x btn btn-outline-danger"><i class="lnr-trash btn-icon-wrapper"></i>Borrar</button>');
    $tagFormLi.append($removeFormButton);

    $removeFormButton.on('click', function () {
        // remove the li for the tag form
        $tagFormLi.remove();
    });
}

function addFormToCollection($collectionHolderClass) {
    // Get the ul that holds the collection of tags
    var $collectionHolder = $('.' + $collectionHolderClass);

    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');
    if (!Number.isInteger(index)) {
        index = $collectionHolder.data('widget-counter') || $collectionHolder.children().length;
    }

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your tags field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label
    // __/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in a li, before the "Add a tag" link li
    var $newFormLi = $('<li></li>').append(newForm);
    // Add the new form at the end of the list
    $collectionHolder.append($newFormLi);
    addFormDeleteLink($newFormLi);
}
